package az.bs.dfoodauth.mapper

/**
 * Interface which contains regular CRUD methods
 */
interface CrudMapperInterface<T,V>  {



    fun findById(id: V): T

    fun findAll(): List<T>

    fun save(element: T): V

    fun removeById(id:V)

    fun update(element: T)
}