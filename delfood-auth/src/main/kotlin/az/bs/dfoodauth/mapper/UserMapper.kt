package az.bs.dfoodauth.mapper

import az.bs.dfoodauth.mapper.CrudMapperInterface
import az.bs.dfoodauth.model.User
import org.apache.ibatis.annotations.*
import org.springframework.stereotype.Component
import javax.websocket.server.PathParam


/**
 * Interface with some MyBatis annotations.
 * MyBatis will use this mapper to gather data from database
 * This is where MyBatis's ORM starts
 */
@Mapper
@Component
interface UserMapper : CrudMapperInterface<User, Long> {

    @Select("select * from dfood_user where id=#{id}")
    override fun findById(@PathParam("id") id: Long): User

    @Select("select * from dfood_user where phone_number=#{pn}")
    fun findByPhoneNumber(@PathParam("pn") phoneNumber: String): User


    @Select("select * from dfood_user")
    override fun findAll(): List<User>

    @Insert("INSERT INTO dfood_user (phone_number, password, salt) VALUES (#{phoneNumber}, #{password}, #{salt}) ")
    override fun save(element: User): Long

    @Delete("delete from dfood_user where id = #{id}")
    override fun removeById(@PathParam("id") id: Long)

    @Delete("delete from dfood_user where phone_number = #{pn}")
    fun removeByPhoneNumber(@PathParam("pn") phoneNumber: String)

    @Update("UPDATE dfood_user SET phoneNumber = #{phoneNumber}, password = #{password}, salt = #{salt}, email = #{email}, role = #{role} WHERE id = #{id}")
    override fun update(element: User)

    @Update("UPDATE dfood_user SET email = #{email} WHERE id = #{id}")
    fun updateEmail(element: User)
}