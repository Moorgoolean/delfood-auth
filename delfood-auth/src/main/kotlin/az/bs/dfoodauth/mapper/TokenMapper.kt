package az.bs.dfoodauth.mapper

import az.bs.dfoodauth.model.Token
import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Select

@Mapper
interface TokenMapper: CrudMapperInterface<Token, Long>{

    @Select("select * from dfood_token where id=#{id}")
    override fun findById(id: Long): Token

    @Select("select * from dfood_token")
    override fun findAll(): List<Token>

    //TODO Famil, help!
    @Insert("INSERT INTO dfood_token (phone_number, password, salt) VALUES (#{phoneNumber}, #{password}, #{salt}) ")
    override fun save(element: Token): Long


    override fun removeById(id: Long)


    override fun update(element: Token)
}