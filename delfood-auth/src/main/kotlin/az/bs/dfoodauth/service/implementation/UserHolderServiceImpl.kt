package az.bs.dfoodauth.service.implementation

import az.bs.dfoodauth.dao.implementation.UserDaoImpl
import az.bs.dfoodauth.model.User
import az.bs.dfoodauth.service.CrudServiceInterface
import az.bs.dfoodauth.service.UserHolderService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserHolderServiceImpl : UserHolderService, CrudServiceInterface<User, Long> {

    @Autowired
    private lateinit var userDaoImpl: UserDaoImpl


    override fun findById(id: Long): User? {
        return userDaoImpl.findById(id)
    }

    override fun findAll(): List<User> {
        return userDaoImpl.findAll()
    }

    override fun save(user: User): Long {
        return userDaoImpl.save(user)
    }

    override fun removeById(id: Long) {
        userDaoImpl.removeById(id)
    }

    override fun update(user: User) {
        userDaoImpl.update(user)
    }


    override fun findByPhoneNumber(phoneNumber: String): User? {
        return userDaoImpl.findByPhoneNumber(phoneNumber)
    }
}

