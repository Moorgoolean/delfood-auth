package az.bs.dfoodauth.service.implementation

import az.bs.dfoodauth.dao.implementation.TokenDaoImpl
import az.bs.dfoodauth.model.Token
import az.bs.dfoodauth.model.status.TokenStatus
import az.bs.dfoodauth.model.status.TokenStatusCode
import az.bs.dfoodauth.service.UserAuthorizationService
import az.bs.dfoodauth.tools.encodeSha256
import az.bs.dfoodauth.tools.globalSalt
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service


@Service
class UserAuthorizationServiceImpl : UserAuthorizationService {
    @Autowired
    lateinit var tokenDaoImpl: TokenDaoImpl

    @Autowired
    lateinit var userHolderServiceImpl: UserHolderServiceImpl

    override fun acquireToken(phoneNumber: String, password: String): TokenStatus {
        // Checks if phone number is registered in database.
        // If user doesn't exist in database, there is no point in giving you a token.
        val user = userHolderServiceImpl.findByPhoneNumber(phoneNumber)
                ?: return TokenStatus(null,TokenStatusCode.TOKEN_NOT_RECEIVED_USER_ERROR)


        //Checking user data.
        if (user.password == encodeSha256("$password${user.salt}$globalSalt")) {
            val token = tokenDaoImpl.findByPhoneNumber(user.phoneNumber)
                    ?: return TokenStatus(tokenDaoImpl.save(Token(user.phoneNumber)).tokenValue,
                            TokenStatusCode.TOKEN_CREATED)

            //Checking if token is expired. Token will be updated if it is.
            if (token.expired) {
                val newToken = Token(phoneNumber)
                tokenDaoImpl.update(newToken)
                return TokenStatus(newToken.tokenValue,TokenStatusCode.TOKEN_UPDATED)
            }

            //If token is up to time, its last updated information is updated.
            token.update()
            tokenDaoImpl.update(token)
            return TokenStatus(token.tokenValue,TokenStatusCode.TOKEN_ACQUIRED)
        }
        //If user entered bad data, we reach this line. So no token will be given. Succ.
        return TokenStatus(null,TokenStatusCode.TOKEN_NOT_RECEIVED_USER_ERROR)
    }


}