package az.bs.dfoodauth.service

import az.bs.dfoodauth.model.status.RegistrationStatus

/**
 * Middle level storage access point
 * Used to push user to User DAO
 */
interface UserRegistrationService {

    fun registerUser(phoneNumber:String, password:String): RegistrationStatus

}