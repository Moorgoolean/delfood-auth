package az.bs.dfoodauth.service

import az.bs.dfoodauth.model.status.TokenStatus

/**
 * Middle level storage access point to Token DAO
 */
interface UserAuthorizationService {

    fun acquireToken(phoneNumber: String, password: String): TokenStatus

}