package az.bs.dfoodauth.service

import az.bs.dfoodauth.model.User

/**
 * Middle level storage access point to Users DAO
 * This one is used as it is not a part of regular CRUD
 */
interface UserHolderService {

    fun findByPhoneNumber(phoneNumber: String): User?

}