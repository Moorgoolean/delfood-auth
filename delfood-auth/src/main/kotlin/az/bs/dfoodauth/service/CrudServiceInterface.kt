package az.bs.dfoodauth.service


/**
 * Middle level storage access point
 * Simple CRUD interface
 * No comments. Just no comments. Please.
 */
interface CrudServiceInterface<T,V> {

    fun findById(id: V): T?

    fun findAll(): List<T>

    fun save(user: T): V

    fun removeById(id:V)

    fun update(user: T)
}