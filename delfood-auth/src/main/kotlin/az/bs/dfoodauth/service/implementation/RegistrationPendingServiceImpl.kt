package az.bs.dfoodauth.service.implementation

import az.bs.dfoodauth.dao.implementation.RegistrationPendingDaoImpl
import az.bs.dfoodauth.service.RegistrationPendingService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class RegistrationPendingServiceImpl : RegistrationPendingService {


    @Autowired
    lateinit var registrationPendingDaoImpl: RegistrationPendingDaoImpl

    override fun registerNewCodeFor(phoneNumber: String): Boolean {
        val result = registrationPendingDaoImpl.findByPhoneNumber(phoneNumber).isNullOrEmpty()
        registrationPendingDaoImpl.save(phoneNumber)
        return result
    }


    override fun compare(phoneNumber: String, code: String): Boolean {
        val receivedCode = registrationPendingDaoImpl.findByPhoneNumber(phoneNumber) ?: return false
        if (receivedCode != code) return false
        return true
    }

    override fun getCode(phoneNumber: String): String? {
        return registrationPendingDaoImpl.findByPhoneNumber(phoneNumber)

    }


}