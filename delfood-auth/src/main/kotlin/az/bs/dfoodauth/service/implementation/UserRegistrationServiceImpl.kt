package az.bs.dfoodauth.service.implementation

import az.bs.dfoodauth.model.User
import az.bs.dfoodauth.model.status.RegistrationStatus
import az.bs.dfoodauth.model.status.RegistrationStatusCode
import az.bs.dfoodauth.service.UserRegistrationService
import az.bs.dfoodauth.tools.encodeSha256
import az.bs.dfoodauth.tools.globalSalt
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class UserRegistrationServiceImpl : UserRegistrationService {
    @Autowired
    private lateinit var userHolderServiceImpl: UserHolderServiceImpl

    override fun registerUser(phoneNumber: String, password: String): RegistrationStatus {
        if (phoneNumber.matches(Regex("\\+994[0-9]{9}")).not() ||
                password.matches(Regex("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$")).not()) {
            return RegistrationStatus(null,
                    RegistrationStatusCode.REGISTRATION_FAILED_WRONG_USER_DATA)
        }

        if (userHolderServiceImpl.findByPhoneNumber(phoneNumber) != null) {
            return RegistrationStatus(null,
                    RegistrationStatusCode.REGISTRATION_FAILED_USER_EXISTS)
        }

        val id = System.currentTimeMillis()
        val salt = UUID.randomUUID().toString()
        val user = User(id, phoneNumber, encodeSha256("$password$salt$globalSalt"), salt)
        userHolderServiceImpl.save(user)

        return RegistrationStatus(user.id,
                RegistrationStatusCode.REGISTRATION_COMPLETED)
    }


}