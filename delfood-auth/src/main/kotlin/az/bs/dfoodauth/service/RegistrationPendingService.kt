package az.bs.dfoodauth.service

/**
 * Middle level storage access point to Registration Pending DAO
 */
interface RegistrationPendingService {

    fun registerNewCodeFor(phoneNumber: String): Boolean

    fun compare(phoneNumber: String, code:String): Boolean

    fun getCode(phoneNumber: String): String?

}