package az.bs.dfoodauth.controller

import az.bs.dfoodauth.model.DTO.RegDataDTO
import az.bs.dfoodauth.model.status.RegistrationStatus
import az.bs.dfoodauth.model.status.RegistrationStatusCode
import az.bs.dfoodauth.model.status.TokenStatus
import az.bs.dfoodauth.service.implementation.RegistrationPendingServiceImpl
import az.bs.dfoodauth.service.implementation.UserAuthorizationServiceImpl
import az.bs.dfoodauth.service.implementation.UserRegistrationServiceImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*


/**
 * This controller is used to do auth and reg actions
 * including intermediate steps
 * Uses REST architecture
 */
@RestController
@RequestMapping("/user_api")
class UserApiController {

    /**
     * Authorization Service is used to authorize users
     */
    @Autowired
    lateinit var userAuthorizationServiceImpl: UserAuthorizationServiceImpl

    /**
     * Registration Service is used to register users
     */
    @Autowired
    lateinit var userRegistrationServiceImpl: UserRegistrationServiceImpl

    /**
     * Registration Pending Service is an intermediate level service which
     * is used to hold secret codes for registration
     */
    @Autowired
    lateinit var registrationPendingServiceImpl: RegistrationPendingServiceImpl

    /**
     * Start registration process
     */
    @PostMapping("/register/start")
    fun startRegister(@RequestBody phoneNumber: String): String {
        return if (registrationPendingServiceImpl.registerNewCodeFor(phoneNumber)) {
            "Code sent"
        } else {
            "Number has already attempted to register recently. New code sent."
        }
    }

    /**
     * Registration completion method pushes new user into
     * database if code is confirmed
     * @value regDataDTO contains phone number, password and secret code
     * @return Result represented by string
     */
    @PostMapping("/register/complete")
    fun completeRegistration(@RequestBody regDataDTO: RegDataDTO): RegistrationStatus {
        val phoneNumber = regDataDTO.phoneNumber
        val password = regDataDTO.password
        val code = regDataDTO.code

        return if (registrationPendingServiceImpl.compare(phoneNumber, code)) {
            userRegistrationServiceImpl.registerUser(phoneNumber, password)
        } else {
            RegistrationStatus(null,RegistrationStatusCode.REGISTRATION_FAILED_WRONG_CODE)
        }
    }

    /**
     * Once you wanted to have your token
     * please, send request along that route
     * @value pass only phoneNumber and password values here. Code value is not in use.
     * @return result represented by string
     */
    @PostMapping("/acquire_token")
    fun acquireToken(@RequestBody regDataDTO: RegDataDTO): TokenStatus {
        val phoneNumber = regDataDTO.phoneNumber
        val password = regDataDTO.password
        return userAuthorizationServiceImpl.acquireToken(phoneNumber, password)
    }

    /**
     * Test method. Will be removed as soon as the db issues are solved.
     *
     * @value phoneNumber is the number you registered with
     * @return corresponding secret code value stored in dao
     */
    @PostMapping("/test_code")
    fun testAcquireCode(@RequestBody phoneNumber: String): String {
        return registrationPendingServiceImpl.getCode(phoneNumber) ?: "No code"
    }
}