package az.bs.dfoodauth.dao.implementation

import az.bs.dfoodauth.dao.UserDao
import az.bs.dfoodauth.model.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class UserDaoImpl : UserDao {


    @Autowired
    lateinit var userHolder: ArrayList<User>


    override fun findById(id: Long): User? {
        return userHolder.find { user -> user.id == id }
    }

    override fun findAll(): List<User> {
        return userHolder
    }

    override fun save(user: User): Long {
        return if (userHolder.add(user)) {
            user.id
        } else -1
    }

    override fun removeById(id: Long) {
        userHolder.removeIf { t: User -> t.id == id }
    }

    override fun update(user: User) {
        userHolder[getIndex(user)] = user
    }

    override fun findByPhoneNumber(phoneNumber: String): User? {
        return userHolder.find { user -> user.phoneNumber == phoneNumber }
    }


    private fun getIndex(user: User): Int {
        return userHolder.indexOf(user)
    }


}
