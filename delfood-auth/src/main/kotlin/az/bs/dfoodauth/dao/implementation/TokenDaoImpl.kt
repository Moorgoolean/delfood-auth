package az.bs.dfoodauth.dao.implementation

import az.bs.dfoodauth.dao.TokenDao
import az.bs.dfoodauth.model.Token
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class TokenDaoImpl : TokenDao {

    @Autowired
    lateinit var tokenHolder: ArrayList<Token>


    override fun findByPhoneNumber(phoneNumber: String): Token? {
        return tokenHolder.find { token -> token.phoneNumber == phoneNumber } ?: return null
    }

    override fun findAll(): List<Token> {
        return tokenHolder
    }

    override fun save(token: Token): Token {
        tokenHolder.add(token)
        return token
    }

    override fun removeByPhoneNumber(phoneNumber: String) {
        tokenHolder.removeIf { token -> token.phoneNumber == phoneNumber }
    }

    override fun update(token: Token): Token {
        tokenHolder[tokenHolder.indexOfFirst { it.phoneNumber == token.phoneNumber }] = token
        return token
    }


}