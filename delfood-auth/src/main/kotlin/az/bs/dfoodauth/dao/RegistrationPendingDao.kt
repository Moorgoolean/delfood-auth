package az.bs.dfoodauth.dao

/**
 * Low level storage access point
 * Secret codes from registration procedure are stored here and read from here
 */
interface RegistrationPendingDao {
    fun save(phoneNumber: String)
    fun findByPhoneNumber(phoneNumber: String): String?
}