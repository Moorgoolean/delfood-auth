package az.bs.dfoodauth.dao

import az.bs.dfoodauth.model.Token

/**
 * Low level storage access point
 * All active tokens are stored here
 * simple CRUD interface
 */
interface TokenDao {


    fun findByPhoneNumber(phoneNumber: String): Token?

    fun findAll(): List<Token>

    fun save(token: Token): Token

    fun removeByPhoneNumber(phoneNumber: String)

    fun update(token: Token): Token

}