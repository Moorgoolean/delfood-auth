package az.bs.dfoodauth.dao.implementation

import az.bs.dfoodauth.dao.RegistrationPendingDao
import az.bs.dfoodauth.mapper.UserMapper
import az.bs.dfoodauth.tools.generateCode
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class RegistrationPendingDaoImpl : RegistrationPendingDao {

    @Autowired
    lateinit var pendingMap: HashMap<String, String>

    @Autowired
    lateinit var userMapper:UserMapper

    override fun save(phoneNumber: String) {
        val contains = pendingMap.containsKey(phoneNumber)
        if (contains) {
            pendingMap[phoneNumber] = generateCode(5)
        } else {
            pendingMap[phoneNumber] = generateCode(5)
        }
    }

    override fun findByPhoneNumber(phoneNumber: String): String? {
        return pendingMap[phoneNumber]
    }




}