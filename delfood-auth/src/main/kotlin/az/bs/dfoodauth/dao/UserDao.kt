package az.bs.dfoodauth.dao

import az.bs.dfoodauth.model.User

/**
 * Low level storage access point
 * Users stored here
 * simple CRUD interface
 */
interface UserDao {

    fun findById(id: Long): User?

    fun findAll(): List<User>

    fun save(user: User): Long

    fun removeById(id: Long)

    fun update(user: User)

    fun findByPhoneNumber(phoneNumber: String): User?
}