package az.bs.dfoodauth.tools

import java.security.MessageDigest

/**
 * This constant is added to each salted password in Users DAO before encoding it with SHA256
 */
const val globalSalt = "MasterOfUniqueness"

/**
 * Thanks to cap, this is SHA256 encoding function
 * Isn't this enough to love kotlin, huh?
 */
fun encodeSha256(value: String): String {
    val bytes = value.toByteArray()
    val md = MessageDigest.getInstance("SHA-256")
    val digest = md.digest(bytes)
    return digest.fold("") { str, it -> str + "%02x".format(it) }
}


