package az.bs.dfoodauth.tools

import java.util.*
import kotlin.streams.asSequence

/**
 * This little guy generates secret codes
 * @length actually i'm not supposed to be cap
 */

fun generateCode(length: Long): String {
    val source = "abcdefghijklmnopqrstuvwxyz123456789"
    return Random().ints(length, 0, source.length)
            .asSequence()
            .map(source::get)
            .joinToString("")
}

