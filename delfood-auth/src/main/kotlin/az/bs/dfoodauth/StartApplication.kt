/**
 * Main application runner class
 * Uses Spring Boot
 * @version 1.0
 * @author Delfood Team
 */

package az.bs.dfoodauth

import az.bs.dfoodauth.model.User
import org.apache.ibatis.type.MappedTypes
import org.mybatis.spring.annotation.MapperScan
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
@MappedTypes(User::class)
@MapperScan("az.bs.dfoodauth.mapper")
class DemoApplication

fun main(args: Array<String>) {
    runApplication<DemoApplication>(*args)
}
