package az.bs.dfoodauth.config

import az.bs.dfoodauth.model.Token
import az.bs.dfoodauth.model.User
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


/**
 * All Spring beans are placed here
 * @Configuration annotation prompts spring to look at these before application runs
 */
@Configuration
class BeanConfig {

    @Bean
    fun getUserHolder(): ArrayList<User> {
        return arrayListOf(User(0, "Moorgoolean", "123pass123ea", "saltTemp"))
    }

    @Bean
    fun getTokenHolder(): ArrayList<Token> {

        return ArrayList()
    }

    @Bean
    fun getPendingMap(): HashMap<String, String> {
        return HashMap()
    }
}