/**
 * Token object
 * Test version
 * Will be improved soon
 */

package az.bs.dfoodauth.model

import java.util.*

/**
 * Period of token's activity time in milliseconds
 * default value is 48 hours which is 172800000 milliseconds
 */
const val EXPIRATION_TIME = 5000


/**
 * Tokens are bounded to user ids.
 * @value userId
 * @value lastUpdated initialized with current time in milliseconds and later
 * is the time you refresh it
 */
data class Token(val phoneNumber: String,
                 val tokenValue: String = UUID.randomUUID().toString(),
                 private var lastUpdated: Long = System.currentTimeMillis()) {

    val expired: Boolean
        get() {
            return (System.currentTimeMillis() - lastUpdated > EXPIRATION_TIME)
        }

    fun update() {
        lastUpdated = System.currentTimeMillis()
    }
}