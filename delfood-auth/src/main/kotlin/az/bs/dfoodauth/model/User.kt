/**
 * User class
 */
package az.bs.dfoodauth.model

/**
 * @id is unique value of user
 * @phoneNumber thanks cap
 * @password hush
 * @salt unique value for user used in password build
 * @email email is optional yet
 * @role role of User (Look at Role enum)
 */
data class User(val id: Long, val phoneNumber: String, var password: String, val salt: String, var email: String? = null, var role:Role = Role.USER_CONSUMER)
