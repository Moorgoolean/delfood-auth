/**
 * Main roles representation enum class
 */
package az.bs.dfoodauth.model

enum class Role {
    ADMIN, USER_MERCHANT, USER_CONSUMER, USER_COURIER
}