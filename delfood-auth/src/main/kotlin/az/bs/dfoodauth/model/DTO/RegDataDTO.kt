/**
 * This class is used to transfer data to controllers
 */
package az.bs.dfoodauth.model.DTO

data class RegDataDTO(val phoneNumber: String, val password: String, val code: String = "")