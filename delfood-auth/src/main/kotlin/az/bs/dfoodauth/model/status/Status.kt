package az.bs.dfoodauth.model.status

import java.util.*

abstract class Status {
    abstract val code:Any
    abstract val resultObject:Any?
}