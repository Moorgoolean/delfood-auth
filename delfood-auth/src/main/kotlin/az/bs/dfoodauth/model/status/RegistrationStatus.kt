package az.bs.dfoodauth.model.status

import com.fasterxml.jackson.annotation.JsonFormat

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
enum class RegistrationStatusCode(val id: Byte,val message: String) {
    REGISTRATION_COMPLETED(0, "Registration completed without any issues"),
    REGISTRATION_FAILED_USER_EXISTS(-1, "Registration failed because your number is already in database."),
    REGISTRATION_FAILED_WRONG_USER_DATA(-2, "Registration failed. Bad input. Either number is entered in incorrect way or password is not in valid format"),
    REGISTRATION_FAILED_WRONG_CODE(-3, "Registration failed. Bad input. You entered the wrong code.")

}

data class RegistrationStatus(override val resultObject: Any?,
                              override val code:RegistrationStatusCode) : Status()