package az.bs.dfoodauth.model.status

import com.fasterxml.jackson.annotation.JsonFormat
import java.util.*

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
enum class TokenStatusCode(val id: Byte, val message: String) {
    TOKEN_CREATED(0, "No previous tokens were found. New token given."),
    TOKEN_ACQUIRED(1, "Token has been found in the database and acquired successfully."),
    TOKEN_UPDATED(2, "Previous token was out of date. Updated."),
    TOKEN_NOT_RECEIVED_USER_ERROR(-1, "Wrong data. No token received."),
    TOKEN_NOT_RECEIVED_SERVER_ERROR(-2, "Internal server error. No token received."),
}

data class TokenStatus(override val resultObject: Any?,
                       override val code: TokenStatusCode) : Status()